import 'dart:io';
import 'package:flutter/material.dart';
import './autofill_onboarding_android.dart';
import './autofill_onboarding_ios.dart';

class AutofillOnboardingScreen extends StatefulWidget {
  static String tag = 'autofill-onboarding-screen';
  @override
  _AutofillOnboardingScreenState createState() =>
      _AutofillOnboardingScreenState();
}

class _AutofillOnboardingScreenState extends State<AutofillOnboardingScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF151f2b),
      body: Platform.isAndroid
          ? AutofillOnboardingAndroid()
          : AutofillOnboardingIOS(),
    );
  }
}
