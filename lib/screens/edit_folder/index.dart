import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';

import 'package:psono/model/datastore.dart' as datastoreModel;
import 'package:psono/model/share.dart' as shareModel;
import 'package:psono/services/manager_datastore_password.dart'
    as managerDatastorePassword;
import 'package:psono/services/manager_share.dart' as managerShare;
import 'package:psono/components/_index.dart' as component;

class EditFolderScreen extends StatefulWidget {
  static String tag = 'edit-folder-screen';
  final datastoreModel.Folder parent;
  final datastoreModel.Datastore datastore;
  final datastoreModel.Folder share;
  final datastoreModel.Folder folder;
  final List<String> path;
  final List<String> relativePath;

  EditFolderScreen({
    this.parent,
    this.datastore,
    this.share,
    this.folder,
    this.path,
    this.relativePath,
  });

  @override
  _EditFolderScreenState createState() => _EditFolderScreenState();
}

class _EditFolderScreenState extends State<EditFolderScreen> {
  var folderName;

  String message;

  @override
  void initState() {
    folderName = TextEditingController(
      text: this.widget.folder.name,
    );
    super.initState();
  }

  @override
  void dispose() {
    folderName?.dispose();
    super.dispose();
  }

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    Widget messageWidget = Container();
    if (message != null) {
      messageWidget = component.AlertInfo(
        text: message,
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(FlutterI18n.translate(
          context,
          'EDIT_FOLDER',
        )),
        elevation: 0,
        backgroundColor: Colors.white,
      ),
      backgroundColor: Color(0xFFebeeef),
      body: new Container(
        padding: new EdgeInsets.all(20.0),
        child: Form(
          key: _formKey,
          child: ListView(
            children: <Widget>[
              new TextFormField(
                controller: folderName,
                validator: (value) {
                  if (value.isEmpty) {
                    return FlutterI18n.translate(
                      context,
                      'FOLDER_NAME_IS_REQUIRED',
                    );
                  }
                  return null;
                },
                decoration: new InputDecoration(
                  labelText: FlutterI18n.translate(
                    context,
                    'FOLDER_NAME',
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: component.BtnSuccess(
                  onPressed: () async {
                    if (!_formKey.currentState.validate()) {
                      return;
                    }

                    List<String> folderPath =
                        new List<String>.from(this.widget.relativePath);
                    folderPath.add(this.widget.folder.id);

                    if (this.widget.folder.shareId != null) {
                      shareModel.Share share = await managerShare.readShare(
                        this.widget.folder.shareId,
                        this.widget.folder.shareSecretKey,
                      );

                      share.folder.name = folderName.text;
                      this.widget.folder.name = folderName.text;
                      await share.save();
                    } else if (this.widget.share == null) {
                      datastoreModel.Datastore datastore =
                          await managerDatastorePassword.getPasswordDatastore(
                        this.widget.datastore.datastoreId,
                      );

                      List<String> pathCopy = new List<String>.from(folderPath);
                      List search = managerDatastorePassword.findInDatastore(
                        pathCopy,
                        datastore.data,
                      );
                      datastoreModel.Folder remoteFolder = search[0][search[1]];

                      remoteFolder.name = folderName.text;
                      this.widget.folder.name = folderName.text;

                      await datastore.save();
                    } else {
                      shareModel.Share share = await managerShare.readShare(
                        this.widget.share.shareId,
                        this.widget.share.shareSecretKey,
                      );

                      List<String> pathCopy = new List<String>.from(
                        folderPath,
                      );
                      List search = managerDatastorePassword.findInDatastore(
                        pathCopy,
                        share.folder,
                      );
                      datastoreModel.Folder remoteFolder = search[0][search[1]];

                      remoteFolder.name = folderName.text;
                      this.widget.folder.name = folderName.text;
                      await share.save();
                    }

                    setState(() {
                      message = FlutterI18n.translate(context, "SAVE_SUCCESS");
                    });
                  },
                  text: FlutterI18n.translate(context, "SAVE"),
                ),
              ),
              messageWidget,
            ],
          ),
        ),
      ),
    );
  }
}
