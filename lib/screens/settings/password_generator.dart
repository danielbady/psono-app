import 'package:flutter/material.dart';
import 'package:flutter_i18n/flutter_i18n.dart';
import 'package:flutter/services.dart';
import 'package:flutter/scheduler.dart';
import 'package:psono/components/_index.dart' as component;
import 'package:psono/model/datastore.dart' as datastoreModel;
import 'package:psono/services/api_client/index.dart' as apiClient;
import 'package:psono/services/manager_datastore_setting.dart'
    as managerDatastoreSetting;
import 'package:psono/services/manager_datastore_user.dart'
    as managerDatastoreUser;
import 'package:psono/redux/actions.dart';
import 'package:psono/redux/store.dart';

class SettingPasswordGenerator extends StatefulWidget {
  static String tag = 'settings-password-generator-screen';

  @override
  _SettingPasswordGeneratorState createState() =>
      _SettingPasswordGeneratorState();
}

class _SettingPasswordGeneratorState extends State<SettingPasswordGenerator> {
  final passwordLength = TextEditingController(
    text: '',
  );
  final lettersUppercase = TextEditingController(
    text: '',
  );
  final lettersLowercase = TextEditingController(
    text: '',
  );
  final numbers = TextEditingController(
    text: '',
  );
  final specialChars = TextEditingController(
    text: '',
  );

  bool enabled = false;
  String message;
  datastoreModel.Datastore datastore;

  Future<void> loadDatastore() async {
    try {
      datastore = await managerDatastoreSetting.getSettingsDatastore();
    } on apiClient.ServiceUnavailableException {
      if (!mounted) return;
      // setState(() {
      //   _screen = 'offline';
      // });
      return;
    } on apiClient.UnauthorizedException {
      managerDatastoreUser.logout();
      Navigator.pushReplacementNamed(context, '/signin/');
      return;
    }

    for (var i = 0; i < datastore.dataKV.length; i++) {
      if (datastore.dataKV[i]['key'] == 'setting_password_length') {
        passwordLength.text = datastore.dataKV[i]['value'];
      }
      if (datastore.dataKV[i]['key'] == 'setting_password_letters_uppercase') {
        lettersUppercase.text = datastore.dataKV[i]['value'];
      }
      if (datastore.dataKV[i]['key'] == 'setting_password_letters_lowercase') {
        lettersLowercase.text = datastore.dataKV[i]['value'];
      }
      if (datastore.dataKV[i]['key'] == 'setting_password_numbers') {
        numbers.text = datastore.dataKV[i]['value'];
      }
      if (datastore.dataKV[i]['key'] == 'setting_password_special_chars') {
        specialChars.text = datastore.dataKV[i]['value'];
      }
    }
  }

  Future<void> initStateAsync() async {
    await loadDatastore();
  }

  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      initStateAsync();
    });
  }

  @override
  void dispose() {
    passwordLength?.dispose();
    lettersUppercase?.dispose();
    lettersLowercase?.dispose();
    numbers?.dispose();
    specialChars?.dispose();
    super.dispose();
  }

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    Widget messageWidget = Container();
    if (message != null) {
      messageWidget = component.AlertInfo(
        text: message,
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(FlutterI18n.translate(
          context,
          'PASSWORD_GENERATOR',
        )),
        elevation: 0,
        backgroundColor: Colors.white,
      ),
      backgroundColor: Color(0xFFebeeef),
      body: new Container(
        padding: new EdgeInsets.all(20.0),
        child: Form(
          key: _formKey,
          child: ListView(
            children: <Widget>[
              new TextFormField(
                controller: passwordLength,
                validator: (value) {
                  return null;
                },
                inputFormatters: <TextInputFormatter>[
                  FilteringTextInputFormatter.digitsOnly,
                ],
                keyboardType: TextInputType.number,
                decoration: new InputDecoration(
                  labelText: FlutterI18n.translate(
                    context,
                    'PASSWORD_LENGTH',
                  ),
                ),
              ),
              new TextFormField(
                controller: lettersUppercase,
                validator: (value) {
                  return null;
                },
                decoration: new InputDecoration(
                  labelText: FlutterI18n.translate(
                    context,
                    'LETTERS_UPPERCASE',
                  ),
                ),
              ),
              new TextFormField(
                controller: lettersLowercase,
                validator: (value) {
                  return null;
                },
                decoration: new InputDecoration(
                  labelText: FlutterI18n.translate(
                    context,
                    'LETTERS_LOWERCASE',
                  ),
                ),
              ),
              new TextFormField(
                controller: numbers,
                validator: (value) {
                  return null;
                },
                inputFormatters: <TextInputFormatter>[
                  FilteringTextInputFormatter.digitsOnly,
                ],
                keyboardType: TextInputType.number,
                decoration: new InputDecoration(
                  labelText: FlutterI18n.translate(
                    context,
                    'NUMBERS',
                  ),
                ),
              ),
              new TextFormField(
                controller: specialChars,
                validator: (value) {
                  return null;
                },
                decoration: new InputDecoration(
                  labelText: FlutterI18n.translate(
                    context,
                    'SPECIAL_CHARS',
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: component.BtnSuccess(
                  onPressed: () async {
                    if (!_formKey.currentState.validate()) {
                      return;
                    }
                    if (datastore != null) {
                      for (var i = 0; i < datastore.dataKV.length; i++) {
                        if (datastore.dataKV[i]['key'] ==
                            'setting_password_length') {
                          datastore.dataKV[i]['value'] = passwordLength.text;
                        }
                        if (datastore.dataKV[i]['key'] ==
                            'setting_password_letters_uppercase') {
                          datastore.dataKV[i]['value'] = lettersUppercase.text;
                        }
                        if (datastore.dataKV[i]['key'] ==
                            'setting_password_letters_lowercase') {
                          datastore.dataKV[i]['value'] = lettersLowercase.text;
                        }
                        if (datastore.dataKV[i]['key'] ==
                            'setting_password_numbers') {
                          datastore.dataKV[i]['value'] = numbers.text;
                        }
                        if (datastore.dataKV[i]['key'] ==
                            'setting_password_special_chars') {
                          datastore.dataKV[i]['value'] = specialChars.text;
                        }
                      }

                      reduxStore.dispatch(
                        new PasswordGeneratorSettingAction(
                          passwordLength.text,
                          lettersUppercase.text,
                          lettersLowercase.text,
                          numbers.text,
                          specialChars.text,
                        ),
                      );

                      await datastore.save();
                    }

                    setState(() {
                      message = FlutterI18n.translate(context, "SAVE_SUCCESS");
                    });
                  },
                  text: FlutterI18n.translate(context, "SAVE"),
                ),
              ),
              messageWidget,
            ],
          ),
        ),
      ),
    );
  }
}
