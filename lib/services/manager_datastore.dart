import 'dart:typed_data';
import 'dart:convert';
import 'package:psono/model/datastore.dart';
import 'package:psono/model/share_right.dart';
import 'package:psono/model/encrypted_data.dart';
import 'package:psono/services/converter.dart' as converter;
import 'package:psono/services/api_client/index.dart' as apiClient;
import 'package:psono/services/crypto_library.dart' as cryptoLibrary;
import 'package:psono/services/manager_base.dart' as managerBase;
import 'package:psono/redux/store.dart';

apiClient.ReadDatastoreList tempDatastoreOverview;
Map<String, Uint8List> tempDatastoreKeyStorage = {};

/// triggered upon logout. Will reset the local cache
logout() {
  tempDatastoreOverview = null;
  tempDatastoreKeyStorage = {};
}

/// Returns the datastore_id for the given type. Usually a cached value is
/// returned if not [forceFresh] is specified.
Future<apiClient.ReadDatastoreList> getDatastoreOverview(
    [bool forceFresh = false]) async {
  if (forceFresh == false && tempDatastoreOverview != null) {
    return tempDatastoreOverview;
  }
  tempDatastoreOverview = await apiClient.readDatastoreList(
    reduxStore.state.token,
    reduxStore.state.sessionSecretKey,
  );
  return tempDatastoreOverview;
}

/// Returns the datastore_id for the given [type]. Usually a cached value is
/// returned if not [forceFresh] is specified.
Future<String> getDatastoreId(String type, [bool forceFresh = false]) async {
  apiClient.ReadDatastoreList datastoresList =
      await getDatastoreOverview(forceFresh);

  for (var i = 0; i < datastoresList.datastores.length; i++) {
    if (datastoresList.datastores[i].type != type ||
        !datastoresList.datastores[i].isDefault) {
      continue;
    }
    return datastoresList.datastores[i].id;
  }

  return '';
}

/// Returns the datastore with the given [datastoreId]
Future<Datastore> getDatastoreWithId(String datastoreId) async {
  apiClient.ReadDatastore datastore = await apiClient.readDatastore(
    reduxStore.state.token,
    reduxStore.state.sessionSecretKey,
    datastoreId,
  );

  String datastoreSecretKeyHex = await managerBase.decryptSecretKey(
      datastore.secretKey, datastore.secretKeyNonce);

  Uint8List datastoreSecretKey = converter.fromHex(datastoreSecretKeyHex);

  tempDatastoreKeyStorage[datastoreId] = datastoreSecretKey;

  Folder data;
  dynamic dataKV;
  if (datastore.data.length > 0) {
    String dataJson = await cryptoLibrary.decryptData(
        datastore.data, datastore.dataNonce, datastoreSecretKey);
    dynamic jsonDecodedData = jsonDecode(dataJson);
    if (jsonDecodedData is Map) {
      data = Folder.fromJson(jsonDecodedData);
    } else {
      dataKV = jsonDecodedData;
    }
  }

  if (dataKV == null && datastore.type == 'settings') {
    dataKV = [];
  }

  if (data == null) {
    data = new Folder(
      datastoreId: datastoreId,
    );
  }

  Datastore decodedDatastore = new Datastore(
    datastoreId: datastoreId,
    type: datastore.type,
    data: data,
    dataKV: dataKV,
    description: datastore.description,
    secretKey: datastoreSecretKey,
    isDefault: datastore.isDefault,
  );

  return decodedDatastore;
}

/// Creates a datastore with the given [type], [description]. [isDefault]
/// specifies whether this datastore is the (new) default datastore for this
/// [type] or not
Future<String> createDatastore(
    String type, String description, bool isDefault) async {
  Uint8List secretKey = await cryptoLibrary.generateSecretKey();
  EncryptedData encryptedDatastoreSecretKey =
      await managerBase.encryptSecretKey(converter.toHex(secretKey));

  apiClient.CreateDatastore createdDatastore = await apiClient.createDatastore(
    reduxStore.state.token,
    reduxStore.state.sessionSecretKey,
    type,
    description,
    Uint8List(0),
    Uint8List(0),
    isDefault,
    encryptedDatastoreSecretKey.text,
    encryptedDatastoreSecretKey.nonce,
  );
  if (tempDatastoreOverview != null) {
    if (isDefault) {
      // New datastore is the new default, so update the existing list
      for (var i = 0; i < tempDatastoreOverview.datastores.length; i++) {
        if (tempDatastoreOverview.datastores[i].type != type) {
          continue;
        }
        tempDatastoreOverview.datastores[i].isDefault = false;
      }
    }

    tempDatastoreOverview.datastores.add(
      new apiClient.ReadDatastoreListEntry(
        id: createdDatastore.datastoreId,
        description: description,
        type: type,
        isDefault: isDefault,
      ),
    );
  }

  return createdDatastore.datastoreId;
}

/// Returns the datastore for the given type and and description
Future<Datastore> getDatastore(String type, [String id]) async {
  if (id == null) {
    id = await getDatastoreId(type);
  }

  if (id == '') {
    id = await createDatastore(type, 'Default', true);
  }

  Datastore datastore = await getDatastoreWithId(id);

  // TODO add implementation

  return datastore;
}

/// Updates the meta of a datastore specified by the datastore id
Future<void> saveDatastoreMeta(
    String datastoreId, String description, bool isDefault) async {
  await apiClient.writeDatastore(
    reduxStore.state.token,
    reduxStore.state.sessionSecretKey,
    datastoreId,
    null,
    null,
    null,
    null,
    description,
    isDefault,
  );

  for (var i = 0; i < tempDatastoreOverview.datastores.length; i++) {
    if (tempDatastoreOverview.datastores[i].id == datastoreId &&
        tempDatastoreOverview.datastores[i].description == description &&
        tempDatastoreOverview.datastores[i].isDefault == isDefault) {
      break;
    }

    if (tempDatastoreOverview.datastores[i].id == datastoreId) {
      tempDatastoreOverview.datastores[i].description = description;
      tempDatastoreOverview.datastores[i].isDefault = isDefault;
    }
    if (tempDatastoreOverview.datastores[i].id != datastoreId && isDefault) {
      tempDatastoreOverview.datastores[i].isDefault = false;
    }
  }

  return;
}

/// Encrypts the content for a datastore with given id. The function will check if the secret key of the
/// datastore is already known, otherwise it will query the server for the details.
Future<EncryptedData> encryptDatastore(
    String datastoreId, dynamic content) async {
  String jsonContent = jsonEncode(content);

  Future<EncryptedData> encrypt(String datastoreId, String jsonContent) async {
    Uint8List secretKey = tempDatastoreKeyStorage[datastoreId];

    return await cryptoLibrary.encryptData(jsonContent, secretKey);
  }

  if (!tempDatastoreKeyStorage.containsKey(datastoreId)) {
    await getDatastoreWithId(datastoreId);
  }

  return await encrypt(datastoreId, jsonContent);
}

/// Saves some content in a datastore
Future<void> saveDatastoreContentWithId(
    String datastoreId, dynamic content) async {
  EncryptedData data = await encryptDatastore(datastoreId, content);

  return await apiClient.writeDatastore(
      reduxStore.state.token,
      reduxStore.state.sessionSecretKey,
      datastoreId,
      data.text,
      data.nonce,
      null,
      null,
      null,
      null);
}

/// Sets the shareRights for folders and items, based on the users rights on the share.
/// Calls recursive itself for all folders and skips nested shares.
updateShareRightsOfFoldersAndItems(
    Folder folder, ShareRight parentShareRights) {
  var n;

  ShareRight shareRights = parentShareRights.clone();

  if (folder.datastoreId != null) {
    // pass
  } else if (folder.shareId != null) {
    shareRights.read = folder.shareRights.read;
    shareRights.write = folder.shareRights.write;
    shareRights.grant = folder.shareRights.grant && folder.shareRights.write;
    shareRights.delete = folder.shareRights.write;
  }

  // check all folders recursive
  if (folder.folders != null) {
    for (n = 0; n < folder.folders.length; n++) {
      // lets not go inside of a new share, and don't touch the shareRights as they will come directly from the share
      if (folder.folders[n].shareId != null) {
        continue;
      }
      folder.folders[n].shareRights = shareRights.clone();
      updateShareRightsOfFoldersAndItems(folder.folders[n], shareRights);
    }
  }

  // check all items
  if (folder.items != null) {
    for (n = 0; n < folder.items.length; n++) {
      if (folder.items[n].shareId != null) {
        continue;
      }
      folder.items[n].shareRights = shareRights.clone();
    }
  }
}
