// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'read_datastore.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReadDatastore _$ReadDatastoreFromJson(Map<String, dynamic> json) {
  return ReadDatastore(
    data: fromHex(json['data'] as String),
    dataNonce: fromHex(json['data_nonce'] as String),
    type: json['type'] as String,
    description: json['description'] as String,
    secretKey: fromHex(json['secret_key'] as String),
    secretKeyNonce: fromHex(json['secret_key_nonce'] as String),
    isDefault: json['is_default'] as bool,
  );
}

Map<String, dynamic> _$ReadDatastoreToJson(ReadDatastore instance) =>
    <String, dynamic>{
      'data': toHex(instance.data),
      'data_nonce': toHex(instance.dataNonce),
      'type': instance.type,
      'description': instance.description,
      'secret_key': toHex(instance.secretKey),
      'secret_key_nonce': toHex(instance.secretKeyNonce),
      'is_default': instance.isDefault,
    };
