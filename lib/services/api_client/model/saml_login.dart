import 'package:json_annotation/json_annotation.dart';
import 'dart:typed_data';
import 'package:psono/services/converter.dart';

part 'saml_login.g.dart';

@JsonSerializable()
class SamlLogin {
  SamlLogin({
    this.loginInfo,
    this.loginInfoNonce,
  });

  @JsonKey(name: 'login_info', fromJson: fromHex, toJson: toHex)
  final Uint8List loginInfo;
  @JsonKey(name: 'login_info_nonce', fromJson: fromHex, toJson: toHex)
  final Uint8List loginInfoNonce;

  factory SamlLogin.fromJson(Map<String, dynamic> json) =>
      _$SamlLoginFromJson(json);

  Map<String, dynamic> toJson() => _$SamlLoginToJson(this);
}
