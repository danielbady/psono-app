// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'create_datastore.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CreateDatastore _$CreateDatastoreFromJson(Map<String, dynamic> json) {
  return CreateDatastore(
    datastoreId: json['datastore_id'] as String,
  );
}

Map<String, dynamic> _$CreateDatastoreToJson(CreateDatastore instance) =>
    <String, dynamic>{
      'datastore_id': instance.datastoreId,
    };
