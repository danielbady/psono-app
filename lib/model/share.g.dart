// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'share.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Share _$ShareFromJson(Map<String, dynamic> json) {
  return Share(
    shareId: json['shareId'] as String,
    shareSecretKey: fromHex(json['shareSecretKey'] as String),
    folder: json['folder'] == null
        ? null
        : Folder.fromJson(json['folder'] as Map<String, dynamic>),
    item: json['item'] == null
        ? null
        : Item.fromJson(json['item'] as Map<String, dynamic>),
    rights: json['rights'] == null
        ? null
        : ShareRight.fromJson(json['rights'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$ShareToJson(Share instance) => <String, dynamic>{
      'shareId': instance.shareId,
      'shareSecretKey': toHex(instance.shareSecretKey),
      'folder': instance.folder,
      'item': instance.item,
      'rights': instance.rights,
    };
