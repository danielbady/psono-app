import 'dart:convert';
import 'dart:typed_data';
import 'package:json_annotation/json_annotation.dart';
import 'package:psono/services/converter.dart';
import 'package:psono/services/manager_datastore_password.dart'
    as managerDatastorePassword;
import 'package:psono/services/manager_datastore.dart' as managerDatastore;
import './share_right.dart';

part 'datastore.g.dart';

typedef FolderCallback = void Function(Folder);
typedef ItemCallback = void Function(Item);

@JsonSerializable()
class Item {
  Item({
    this.id,
    this.name,
    this.deleted,
    this.type,
    this.urlfilter,
    this.secretId,
    this.fileChunks,
    this.fileId,
    this.fileTitle,
    this.fileRepositoryId,
    this.fileShardId,
    this.fileSecretKey,
    this.secretKey,
    this.shareId,
    this.shareSecretKey,
    this.shareRights,
  });

  @JsonKey(includeIfNull: false)
  String id;
  @JsonKey(includeIfNull: false)
  String name;
  @JsonKey(includeIfNull: false)
  bool deleted;
  @JsonKey(includeIfNull: false)
  String type;
  @JsonKey(includeIfNull: false)
  String urlfilter;
  @JsonKey(name: 'secret_id', includeIfNull: false)
  String secretId;
  @JsonKey(name: 'file_chunks', includeIfNull: false)
  Map<int, String> fileChunks;
  @JsonKey(name: 'file_id', includeIfNull: false)
  String fileId;
  @JsonKey(name: 'file_title', includeIfNull: false)
  String fileTitle;
  @JsonKey(name: 'file_repository_id', includeIfNull: false)
  String fileRepositoryId;
  @JsonKey(name: 'file_shard_id', includeIfNull: false)
  String fileShardId;
  @JsonKey(
      name: 'file_secret_key',
      fromJson: fromHex,
      toJson: toHex,
      includeIfNull: false)
  Uint8List fileSecretKey;
  @JsonKey(
      name: 'secret_key',
      fromJson: fromHex,
      toJson: toHex,
      includeIfNull: false)
  Uint8List secretKey;
  @JsonKey(name: 'share_id', includeIfNull: false)
  String shareId;
  @JsonKey(
      name: 'share_secret_key',
      fromJson: fromHex,
      toJson: toHex,
      includeIfNull: false)
  Uint8List shareSecretKey;
  @JsonKey(ignore: true, includeIfNull: false)
  ShareRight shareRights;

  Item clone() {
    Item newItem = new Item(
      id: this.id,
      name: this.name,
      deleted: this.deleted,
      type: this.type,
      urlfilter: this.urlfilter,
      secretId: this.secretId,
      fileChunks: this.fileChunks,
      fileId: this.fileId,
      fileTitle: this.fileTitle,
      fileRepositoryId: this.fileRepositoryId,
      fileShardId: this.fileShardId,
      secretKey:
          (this.secretKey == null) ? null : Uint8List.fromList(this.secretKey),
      fileSecretKey: (this.fileSecretKey == null)
          ? null
          : Uint8List.fromList(this.fileSecretKey),
      shareId: this.shareId,
      shareSecretKey: (this.shareSecretKey == null)
          ? null
          : Uint8List.fromList(this.shareSecretKey),
      shareRights: (this.shareRights == null) ? null : this.shareRights.clone(),
    );

    return newItem;
  }

  factory Item.fromJson(Map<String, dynamic> json) => _$ItemFromJson(json);

  Map<String, dynamic> toJson() => _$ItemToJson(this);
}

@JsonSerializable()
class ShareLocation {
  ShareLocation({
    this.secretKey,
    this.paths,
  });

  @JsonKey(name: 'secret_key', fromJson: fromHex, toJson: toHex)
  Uint8List secretKey;
  List<List<String>> paths;

  ShareLocation clone() {
    ShareLocation newShareLocation = new ShareLocation(
      secretKey:
          (this.secretKey == null) ? null : Uint8List.fromList(this.secretKey),
    );

    if (this.paths != null) {
      newShareLocation.paths = [];
      for (var i = 0; i < this.paths.length; i++) {
        newShareLocation.paths.add(this.paths[i]);
      }
    } else {
      newShareLocation.paths = null;
    }

    return newShareLocation;
  }

  factory ShareLocation.fromJson(Map<String, dynamic> json) =>
      _$ShareLocationFromJson(json);

  Map<String, dynamic> toJson() => _$ShareLocationToJson(this);
}

typedef void SaveFunction();

@JsonSerializable()
class Folder {
  Folder({
    this.id,
    this.datastoreId,
    this.folders,
    this.items,
    this.name,
    this.deleted,
    this.shareId,
    this.shareSecretKey,
    this.shareIndex,
    this.shareRights,
  });

  @JsonKey(includeIfNull: false)
  String id;
  @JsonKey(name: 'datastore_id', includeIfNull: false)
  String datastoreId;
  @JsonKey(includeIfNull: false)
  List<Folder> folders = [];
  @JsonKey(includeIfNull: false)
  List<Item> items = [];
  @JsonKey(includeIfNull: false)
  String name;
  @JsonKey(includeIfNull: false)
  bool deleted;
  @JsonKey(name: 'share_id', includeIfNull: false)
  String shareId;
  @JsonKey(
      name: 'share_secret_key',
      fromJson: fromHex,
      toJson: toHex,
      includeIfNull: false)
  Uint8List shareSecretKey;
  @JsonKey(name: 'share_index', includeIfNull: false)
  Map<String, ShareLocation> shareIndex;
  @JsonKey(ignore: true, includeIfNull: false)
  ShareRight shareRights;

  Folder clone() {
    Folder newFolder = new Folder(
      id: this.id,
      datastoreId: this.datastoreId,
      name: this.name,
      shareId: this.shareId,
      shareSecretKey: (this.shareSecretKey == null)
          ? null
          : Uint8List.fromList(this.shareSecretKey),
      shareRights: (this.shareRights == null) ? null : this.shareRights.clone(),
    );

    if (this.deleted == true) {
      newFolder.deleted = true;
    }

    if (this.items != null) {
      newFolder.items = [];
      for (var i = 0; i < this.items.length; i++) {
        newFolder.items
            .add((this.items[i] == null) ? null : this.items[i].clone());
      }
    } else {
      newFolder.items = null;
    }

    if (this.folders != null) {
      newFolder.folders = [];
      for (var i = 0; i < this.folders.length; i++) {
        newFolder.folders
            .add((this.folders[i] == null) ? null : this.folders[i].clone());
      }
    } else {
      newFolder.folders = null;
    }

    if (this.shareIndex != null) {
      newFolder.shareIndex = {};
      this.shareIndex.forEach((String key, ShareLocation value) {
        newFolder.shareIndex[key] = (value == null) ? null : value.clone();
      });
    } else {
      newFolder.shareIndex = null;
    }

    return newFolder;
  }

  factory Folder.fromJson(Map<String, dynamic> json) => _$FolderFromJson(json);

  Map<String, dynamic> toJson() => _$FolderToJson(this);
}

/// Represents a datastore
@JsonSerializable()
class Datastore {
  Datastore({
    this.datastoreId,
    this.type,
    this.description,
    this.secretKey,
    this.isDefault,
    this.data,
    this.dataKV,
  });

  final String datastoreId;
  final String type;
  final Folder data;
  final dynamic dataKV;
  final String description;
  @JsonKey(fromJson: fromHex, toJson: toHex)
  final Uint8List secretKey;
  final bool isDefault;

  Datastore clone() {
    Datastore newDatastore = new Datastore(
      datastoreId: this.datastoreId,
      type: this.type,
      description: this.description,
      secretKey:
          (this.secretKey == null) ? null : Uint8List.fromList(this.secretKey),
      isDefault: this.isDefault,
      data: (this.data == null) ? null : this.data.clone(),
      dataKV:
          (this.dataKV == null) ? null : jsonDecode(jsonEncode(this.dataKV)),
    );

    return newDatastore;
  }

  save() async {
    if (this.dataKV == null) {
      Folder folderCopy = (this.data == null) ? null : this.data.clone();
      managerDatastorePassword.hideSubShareContent(folderCopy);
      return await managerDatastore.saveDatastoreContentWithId(
        this.datastoreId,
        folderCopy,
      );
    } else {
      return await managerDatastore.saveDatastoreContentWithId(
        this.datastoreId,
        this.dataKV,
      );
    }
  }
}
