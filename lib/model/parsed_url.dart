import 'package:quiver/core.dart';

class ParsedUrl {
  ParsedUrl({
    this.scheme,
    this.authority,
    this.fullDomain,
    this.topDomain,
    this.port,
    this.path,
    this.query,
    this.fragment,
  });

  final String scheme;
  final String authority;
  final String fullDomain;
  final String topDomain;
  final int port;
  final String path;
  final String query;
  final String fragment;

  bool operator ==(o) {
    return o is ParsedUrl &&
        o.scheme == scheme &&
        o.authority == authority &&
        o.fullDomain == fullDomain &&
        o.topDomain == topDomain &&
        o.port == port &&
        o.path == path &&
        o.query == query &&
        o.fragment == fragment;
  }

  int get hashCode => hash2(hash4(scheme, authority, fullDomain, topDomain),
      hash4(port, path, query, fragment));
}
