// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_result_decrypted.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) {
  return User(
    username: json['username'] as String,
    publicKey: fromHex(json['public_key'] as String),
    privateKey: fromHex(json['private_key'] as String),
    privateKeyNonce: fromHex(json['private_key_nonce'] as String),
    userSauce: json['user_sauce'] as String,
  );
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'username': instance.username,
      'public_key': toHex(instance.publicKey),
      'private_key': toHex(instance.privateKey),
      'private_key_nonce': toHex(instance.privateKeyNonce),
      'user_sauce': instance.userSauce,
    };

LoginResultDecrypted _$LoginResultDecryptedFromJson(Map<String, dynamic> json) {
  return LoginResultDecrypted(
    token: json['token'] as String,
    requiredMultifactors: (json['required_multifactors'] as List)
        ?.map((e) => e as String)
        ?.toList(),
    sessionPublicKey: fromHex(json['session_public_key'] as String),
    sessionSecretKey: fromHex(json['session_secret_key'] as String),
    sessionSecretKeyNonce: fromHex(json['session_secret_key_nonce'] as String),
    password: json['password'] as String,
    userValidator: fromHex(json['user_validator'] as String),
    userValidatorNonce: fromHex(json['user_validator_nonce'] as String),
    user: json['user'] == null
        ? null
        : User.fromJson(json['user'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$LoginResultDecryptedToJson(
        LoginResultDecrypted instance) =>
    <String, dynamic>{
      'token': instance.token,
      'required_multifactors': instance.requiredMultifactors,
      'session_public_key': toHex(instance.sessionPublicKey),
      'session_secret_key': toHex(instance.sessionSecretKey),
      'session_secret_key_nonce': toHex(instance.sessionSecretKeyNonce),
      'password': instance.password,
      'user_validator': toHex(instance.userValidator),
      'user_validator_nonce': toHex(instance.userValidatorNonce),
      'user': instance.user,
    };
