// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'known_host.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

KnownHost _$KnownHostFromJson(Map<String, dynamic> json) {
  return KnownHost(
    url: json['url'] as String,
    verifyKey: fromHex(json['verifyKey'] as String),
  );
}

Map<String, dynamic> _$KnownHostToJson(KnownHost instance) => <String, dynamic>{
      'url': instance.url,
      'verifyKey': toHex(instance.verifyKey),
    };
