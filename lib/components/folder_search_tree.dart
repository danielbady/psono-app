import 'package:flutter/material.dart';
import './folder.dart' as componentFolder;
import './item.dart' as componentItem;
import 'package:psono/model/datastore.dart' as datastoreModel;
import 'package:psono/services/helper.dart' as helper;
import 'package:psono/services/autofill.dart' as autofillService;
import 'package:psono/screens/edit_secret/index.dart';
import 'package:psono/screens/folder/index.dart';

class FolderSearchTree extends StatelessWidget {
  final datastoreModel.Folder root;
  final List<datastoreModel.Folder> filteredFolders = [];
  final List<List<String>> filteredFolderPaths = [];
  final List<datastoreModel.Item> filteredItems = [];
  final datastoreModel.Datastore datastore;
  final datastoreModel.Folder share;
  final List<String> path;
  final List<String> relativePath;
  final String search;
  final bool autofill;

  FolderSearchTree({
    this.root,
    this.datastore,
    this.share,
    this.path,
    this.relativePath,
    this.search,
    this.autofill,
  });

  @override
  Widget build(BuildContext context) {
    Function filter = helper.getPasswordFilter(search);

    void filterFolder(datastoreModel.Folder root, List<String> path) {
      if (root == null) {
        return;
      }

      if (root.folders != null) {
        for (var i = 0; i < root.folders.length; i++) {
          if (filter(root.folders[i]) && root.folders[i].deleted != true) {
            filteredFolders.add(root.folders[i]);
            filteredFolderPaths.add(path);
          }

          if (root.folders[i].deleted != true) {
            filterFolder(root.folders[i],
                List<String>.from(path)..addAll([root.folders[i].id]));
          }
        }
      }
      if (root.items != null) {
        for (var i = 0; i < root.items.length; i++) {
          if (filter(root.items[i]) && root.items[i].deleted != true) {
            filteredItems.add(root.items[i]);
          }
        }
      }
      filteredFolders.sort((a, b) => a.name.compareTo(b.name));
      filteredItems.sort((a, b) => a.name.compareTo(b.name));
    }

    filterFolder(root, path);

    int _getFolderCount() {
      return filteredFolders.length;
    }

    int _getItemCount() {
      return filteredItems.length;
    }

    int _calculateEntryCount() {
      return _getFolderCount() + _getItemCount();
    }

    return SliverGrid(
      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
        maxCrossAxisExtent: 150.0,
        mainAxisSpacing: 10.0,
        crossAxisSpacing: 10.0,
        childAspectRatio: 1.0,
      ),
      delegate: SliverChildBuilderDelegate(
        (BuildContext context, int index) {
          int folderCount = _getFolderCount();
          if (index < folderCount) {
            datastoreModel.Folder folder = filteredFolders[index];
            return componentFolder.Folder(
              folder: folder,
              onPressed: () async {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => FolderScreen(
                      autoNavigate: [],
                      folder: folder,
                      datastore: datastore,
                      share: share,
                      path: List.from(filteredFolderPaths[index])
                        ..addAll([folder.id]),
                      relativePath: List.from(relativePath)
                        ..addAll([folder.id]),
                      autofill: autofill,
                    ),
                  ),
                );
              },
              isShare: folder.shareId != null,
              color: Color(0xFF151f2b),
            );
          } else {
            datastoreModel.Item item = filteredItems[index - folderCount];
            return componentItem.Item(
              item: item,
              onPressed: () async {
                if (autofill) {
                  await autofillService.autofill(item.secretId, item.secretKey);
                } else {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => EditSecretScreen(
                        parent: root,
                        datastore: datastore,
                        share: share,
                        item: item,
                        path: path,
                        relativePath: relativePath,
                      ),
                    ),
                  );
                }
              },
              isShare: item.shareId != null,
              color: Color(0xFF151f2b),
            );
          }
        },
        childCount: _calculateEntryCount(),
      ),
    );
  }
}
