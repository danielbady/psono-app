import 'package:flutter/material.dart';
import './folder.dart' as componentFolder;
import './item.dart' as componentItem;
import 'package:psono/model/datastore.dart' as datastoreModel;

class FolderSelectTree extends StatelessWidget {
  final datastoreModel.Folder root;
  final datastoreModel.Datastore datastore;
  final datastoreModel.Folder share;
  final List<String> path;
  final List<String> relativePath;
  final List<String> selectedFolders;
  final List<String> selectedItems;
  final datastoreModel.FolderCallback onSelectFolder;
  final datastoreModel.ItemCallback onSelectItem;
  final List<datastoreModel.Folder> filteredFolders = [];
  final List<datastoreModel.Item> filteredItems = [];

  FolderSelectTree(
      {this.root,
      this.datastore,
      this.share,
      this.path,
      this.relativePath,
      this.selectedFolders,
      this.selectedItems,
      this.onSelectFolder,
      this.onSelectItem});

  @override
  Widget build(BuildContext context) {
    bool filterDeleted(datastoreEntry) {
      return datastoreEntry.deleted != true;
    }

    void filterContent(datastoreModel.Folder root, List<String> path) {
      if (root == null) {
        return;
      }

      if (root.folders != null) {
        for (var i = 0; i < root.folders.length; i++) {
          if (filterDeleted(root.folders[i])) {
            filteredFolders.add(root.folders[i]);
          }
        }
      }
      if (root.items != null) {
        for (var i = 0; i < root.items.length; i++) {
          if (filterDeleted(root.items[i])) {
            filteredItems.add(root.items[i]);
          }
        }
      }
      filteredFolders.sort((a, b) {
        if (a.name == null) {
          return -1;
        }
        if (b.name == null) {
          return 1;
        }
        return a.name.compareTo(b.name);
      });
      filteredItems.sort((a, b) {
        if (a.name == null) {
          return -1;
        }
        if (b.name == null) {
          return 1;
        }
        return a.name.compareTo(b.name);
      });
    }

    filterContent(root, path);

    int _getFolderCount() {
      return filteredFolders.length;
    }

    int _getItemCount() {
      return filteredItems.length;
    }

    int _calculateEntryCount() {
      return _getFolderCount() + _getItemCount();
    }

    return SliverGrid(
      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
        maxCrossAxisExtent: 150.0,
        mainAxisSpacing: 10.0,
        crossAxisSpacing: 10.0,
        childAspectRatio: 1.0,
      ),
      delegate: SliverChildBuilderDelegate(
        (BuildContext context, int index) {
          int folderCount = _getFolderCount();
          if (index < folderCount) {
            datastoreModel.Folder folder = filteredFolders[index];
            return componentFolder.Folder(
              folder: folder,
              onPressed: () async {
                if (onSelectFolder != null) {
                  onSelectFolder(folder);
                }
              },
              isShare: folder.shareId != null,
              color: Color(0xFF151f2b),
              showSelectBox: true,
              isSelected: selectedFolders.contains(folder.id),
            );
          } else {
            datastoreModel.Item item = filteredItems[index - folderCount];
            return componentItem.Item(
              item: item,
              onPressed: () async {
                if (onSelectItem != null) {
                  onSelectItem(item);
                }
              },
              isShare: item.shareId != null,
              color: Color(0xFF151f2b),
              showSelectBox: true,
              isSelected: selectedItems.contains(item.id),
            );
          }
        },
        childCount: _calculateEntryCount(),
      ),
    );
  }
}
