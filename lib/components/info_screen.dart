import 'package:flutter/material.dart';
import './alert_info.dart';

class InfoScreen extends StatelessWidget {
  final String text;
  final List<Widget> buttons;

  InfoScreen({this.text, this.buttons = const []});

  @override
  Widget build(BuildContext context) {
    final logo = Hero(
      tag: 'hero',
      child: Image.asset('assets/images/logo.png'),
    );

    return Center(
      child: ListView(
        shrinkWrap: true,
        padding: EdgeInsets.only(left: 24.0, right: 24.0),
        children: <Widget>[
          logo,
          SizedBox(height: 32.0),
          AlertInfo(
            text: text,
          ),
          SizedBox(height: 8.0),
          Row(
            children: buttons,
          ),
        ],
      ),
    );
  }
}
