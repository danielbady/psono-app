import 'package:psono/app_config.dart';
import 'package:psono/main_core.dart';
import 'package:flutter/material.dart';
import 'dart:async';

Future<void> main() async {
  var configuredApp = new AppConfig(
    appName: 'Psono Dev',
    flavorName: 'development',
    child: new MyApp(),
  );
  runApp(configuredApp);
}
