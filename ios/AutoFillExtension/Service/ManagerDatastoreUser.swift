//
//  ManagerDatastoreUser.swift
//  AutoFillExtension
//

import Foundation

class ManagerDatastoreUser {
    public func logout() {
        storage.delete(key: "token")
        storage.delete(key: "sessionSecretKeye")
        storage.delete(key: "secretKey")
        storage.delete(key: "privateKey")
    }
}

let managerDatastoreUser = ManagerDatastoreUser()
