//
//  Helper.swift
//  AutoFillExtension
//

import Foundation

class HelperService {
    /**
     Takes a folder and an array for the result. Will go through the folder recursive and fill the array with all the items it finds
     
     - Parameter obj: The folder
     - Parameter list: The list that will be filled with the items
     */
    public func createList(obj: Folder, list: inout [Item]) {
        
        if obj.items != nil {
            for item in obj.items! {
                list.append(item)
            }
        }
        
        if obj.folders != nil {
            for folder in obj.folders! {
                createList(obj: folder, list: &list)
            }
        }
    }
    /**
     Will create a filter that can be aop
     
     - Parameter obj: The folder
     - Parameter list: The list that will be filled with the items
     
     - Returns: Returns a filter function
     */
    public func getPasswordFilter(test: String) -> (Item) -> Bool {
        let searchStrings = test.lowercased().split(separator: " ")
        
        func filter(datastoreEntry: Item) -> Bool {
            var containCounter = 0;
            
            for searchString in searchStrings {
                if datastoreEntry.name == nil {
                    continue
                }
                if datastoreEntry.name!.lowercased().contains(searchString) {
                    containCounter += 1
                    continue
                } else if datastoreEntry.id != nil && datastoreEntry.id! == searchString {
                    containCounter += 1
                    continue
                } else if datastoreEntry.shareId != nil && datastoreEntry.shareId! == searchString {
                    containCounter += 1
                    continue
                }
                
                if datastoreEntry.secretId != nil && datastoreEntry.secretId! == searchString {
                   containCounter += 1
                   continue
                } else if datastoreEntry.urlfilter != nil && datastoreEntry.urlfilter!.contains(searchString) {
                    containCounter += 1
                    continue
                 }
                
            }
            
            return containCounter == searchStrings.count
        }
        
        return filter
    }
}

let helper = HelperService()
